import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharacterComponent } from './components/character/character.component';
import { DetailCharacterComponent } from './pages/detail-character/detail-character.component';
import { ListCharactersComponent } from './pages/list-characters/list-characters.component';

@NgModule({
  declarations: [
    AppComponent,
    CharacterComponent,
    DetailCharacterComponent,
    ListCharactersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
