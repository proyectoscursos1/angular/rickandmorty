import { Character } from './../interfaces/character.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CharacterService {
    API_URL = 'https://rickandmortyapi.com/api/character';

constructor(private http: HttpClient) { }

getCharacters(){
    return this.http.get<Character[]>(this.API_URL).pipe(map((res: any) => res.results));    
}

}
