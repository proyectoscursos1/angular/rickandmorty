import { CharacterService } from './../../services/character.service';
import { Character } from './../../interfaces/character.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-characters',
  templateUrl: './list-characters.component.html',
  styleUrls: ['./list-characters.component.css']
})
export class ListCharactersComponent implements OnInit {
  characters: Character[] = [];

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    // this.characters = [];
    this.allCharacters();
  }

  allCharacters(){
    this.characterService.getCharacters().subscribe(res => {
      console.log(res);
      this.characters = res;
      // console.log('msj')
    })
  }
}
